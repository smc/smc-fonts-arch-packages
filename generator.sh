#!/bin/sh
# Generates ttf-malayalam-fonts packages for arch

### Edit the font details as required ###

FLAVORS=( "arch" )

FONTS=( "AnjaliOldLipi" "Chilanka" "Dyuthi" "Keraleeyam" "Manjari" "Meera" "Rachana" "RaghuMalayalamSans" "Suruma" )

fontdetails () {
  case $1 in
    AnjaliOldLipi)
      echo "7.0.0+20170909" "67" "OFL1.1" "This is Anjali Old Lipi, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Chilanka)
      echo "1.3+20180106" "67" "OFL1.1" "This is Chilanka, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Dyuthi)
      echo "2.0.0+20180306" "67" "OFL1.1" "This is Dyuthi, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Keraleeyam)
      echo "2.0.0+20170909" "67" "OFL1.1" "This is Keraleeyam, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Manjari)
      echo "1.4" "65-0" "OFL1.1" "Malayalam unicode font with rounded terminals suitable for body text."
      ;;
    Meera)
      echo "7.0.0+20171009" "65-0" "OFL1.1" "This is Meera, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Rachana)
      echo "7.0.0+20170908" "65-0" "OFL1.1" "This is Rachana, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    RaghuMalayalamSans)
      echo "2.0.1" "67" "GPL2" "This is RaghuMalayalamSans, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    Suruma)
      echo "6.1.1" "67" "GPL3" "This is Suruma, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
      ;;
    *)
      echo "0" "0" "" "Random"
      ;;
  esac
}

## End of font details. Don't edit past this ##

if [ "$1" == "clean" ]; then
  echo "Cleaning up existing files"
  for FLAVOR in "${FLAVORS[@]}"
  do
    rm -rf "$FLAVOR"
  done
fi

pkgbuildtemplate (){
  FULLNAME=$1
  NAME=`echo $FULLNAME| tr '[:upper:]' '[:lower:]'`
  PKGVER=$2
  CONFPREFIX=$3
  PKGDESC=$4
  LICENSE=$5
  PKGREL=1
  #if [ -e $6 ]; then
  #  PKGREL=1
  #else
  #  PKGREL=$7
  #fi

#define the template.
cat << EOF
# Contributor: Aashik S  aashiks at gmail dot com
# Maintainer: http://smc.org.in
# Contributor: Akshay S Dinesh asdofindia at gmail dot com
# Contributor: Jishnu Mohan jishnu7 at gmail dot com

pkgname=ttf-malayalam-font-$NAME
pkgver=$PKGVER
pkgrel=$PKGREL
pkgdesc="$PKGDESC"
arch=(any)
url="http://smc.org.in/fonts/"
license=("$LICENSE")
depends=(fontconfig xorg-font-utils)
source=("http://smc.org.in/downloads/fonts/$NAME/$FULLNAME.ttf"
        "https://gitlab.com/smc/$NAME/raw/master/$CONFPREFIX-smc-$NAME.conf")
sha256sums=('updpkgsums'
         'updpkgsums')
install=ttf-malayalam-fonts.install


package() {
  mkdir -p "\${pkgdir}/usr/share/fonts/TTF" || return 1
  install -m644 *.ttf "\${pkgdir}/usr/share/fonts/TTF"
  mkdir -p "\${pkgdir}/etc/fonts/conf.d" || return 1
  install *.conf "\${pkgdir}/etc/fonts/conf.d" || return 1
}
EOF
}

installfiletemplate () {
cat << EOF
post_install() {
  echo "Running fc-cache to update font cache"
  fc-cache -fs
  mkfontscale usr/share/fonts/TTF
  mkfontdir usr/share/fonts/TTF
  echo "Finished updating font cache"
}

post_upgrade() {
  post_install
}

post_remove() {
  post_install
}
EOF
}

metatemplate () {
cat << EOF
# Contributor: Aashik S  aashiks at gmail dot com
# Maintainer: http://smc.org.in
# Contributor: Akshay S Dinesh asdofindia at gmail dot com

pkgname=ttf-malayalam-fonts-meta
pkgver=6.1
pkgrel=1
pkgdesc="This is a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
arch=(any)
url="http://smc.org.in/fonts/"

depends=(ttf-malayalam-font-anjalioldlipi ttf-malayalam-font-chilanka ttf-malayalam-font-dyuthi
ttf-malayalam-font-keraleeyam ttf-malayalam-font-meera ttf-malayalam-font-rachana ttf-malayalam-font-raghumalayalamsans ttf-malayalam-font-suruma )

package(){
return 0
}
EOF
}

packagedir () {
  cd $1
  updpkgsums
  makepkg --source -f
  makepkg --printsrcinfo > .SRCINFO
  cd -
}

for FLAVOR in "${FLAVORS[@]}"
do
  for FONT in "${FONTS[@]}"
  do
    NAME=`echo $FONT| tr '[:upper:]' '[:lower:]'`
    read PKGVER CONFPREFIX LICENSE PKGDESC <<<$(fontdetails $FONT)
    mkdir -p $FLAVOR/ttf-malayalam-font-$NAME/
    pkgbuildtemplate $FONT $PKGVER $CONFPREFIX "$PKGDESC" $LICENSE > $FLAVOR/ttf-malayalam-font-$NAME/PKGBUILD
    installfiletemplate > $FLAVOR/ttf-malayalam-font-$NAME/ttf-malayalam-font-$NAME.install
    packagedir $FLAVOR/ttf-malayalam-font-$NAME/
  done
  mkdir -p $FLAVOR/ttf-malayalam-fonts-meta/
  metatemplate > $FLAVOR/ttf-malayalam-fonts-meta/PKGBUILD
  packagedir $FLAVOR/ttf-malayalam-fonts-meta/
done
